import { Component, OnDestroy, OnInit } from '@angular/core';
import { SignaturesContentService } from '../../service/signatures.service';
import { NotificationService } from '../../service/notification.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { map, finalize, tap, catchError } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../service/auth.service';
import { AlertController, ModalController, PopoverController } from '@ionic/angular';
import { of } from 'rxjs';
import { OtpService } from '../../document/visa-workflow/otps/otp.service';
import { FunctionsService } from '../../service/functions.service';

declare let tinymce: any;

export interface Connector {
    id: string;
    type: string;
    label: string;
    apiUri: string;
    apiKey: string;
    securityModes: string[];
    message: any;
}

@Component({
    selector: 'app-administration-otp',
    templateUrl: 'otp.component.html',
    styleUrls: ['../administration.scss', 'otp.component.scss'],
})

export class OtpComponent implements OnInit, OnDestroy {

    creationMode: boolean = true;
    loading: boolean = true;
    connector: Connector;
    connectorClone: Connector;
    title: string = '';

    apiDefaultUri = {
        yousign : 'https://api.yousign.com/'
    };

    securityModes: string[] =  [
        'sms',
        'email'
    ];

    tags = [];

    connectorTypes: any[] = [];
    nbCurrentWorkflow: number;

    constructor(
        public http: HttpClient,
        private translate: TranslateService,
        private route: ActivatedRoute,
        private router: Router,
        public signaturesService: SignaturesContentService,
        public notificationService: NotificationService,
        public dialog: MatDialog,
        public authService: AuthService,
        public popoverController: PopoverController,
        public modalController: ModalController,
        public alertController: AlertController,
        public otpService: OtpService,
        private functions: FunctionsService
    ) {
        this.getConnectorTypes();
        this.connector = {
            id: '',
            type: this.connectorTypes[0].id,
            label: '',
            apiUri: this.apiDefaultUri[this.connectorTypes[0]],
            apiKey: '',
            securityModes: ['sms'],
            message : {
                otp_sms : '',
                notification : {
                    subject: '',
                    body: ''
                }
            }
        };
    }

    ngOnInit(): void {
        this.route.params.subscribe(async (params: any) => {
            this.initTags();
            if (params['id'] === undefined) {
                this.creationMode = true;
                this.title = this.translate.instant('lang.otpConnectorCreation');
                this.loading = false;
                this.connectorClone = JSON.parse(JSON.stringify(this.connector));
                setTimeout(() => {
                    this.initMce();
                }, 0);
            } else {
                this.creationMode = false;
                await this.getConfig(params['id']);
            }
        });
    }

    ngOnDestroy(): void {
        tinymce.remove();
    }

    initMce() {
        const param = {
            selector: '#email_message',
            base_url: this.functions.getBaseUrl() + '/tinymce/',
            height: '200',
            suffix: '.min',
            extended_valid_elements : 'tag,class',
            content_css: this.functions.getBaseUrl() + '/assets/custom_tinymce.css',
            language: this.translate.instant('lang.langISO').replace('-', '_'),
            language_url: `../node_modules/tinymce-i18n/langs/${this.translate.instant('lang.langISO').replace('-', '_')}.js`,
            menubar: false,
            statusbar: false,
            readonly: false,
            plugins: [
                'autolink', 'table', 'code', 'noneditable'
            ],
            noneditable_noneditable_class: 'mceNonEditable',
            table_toolbar: '',
            table_sizing_mode: 'relative',
            table_resize_bars: false,
            toolbar_sticky: true,
            toolbar_drawer: 'floating',
            table_style_by_css: true,
            content_style: 'table td { padding: 1px; vertical-align: top; }',
            forced_root_block : false,
            toolbar: 'undo redo | fontselect fontsizeselect | bold italic underline strikethrough forecolor | table maarch_b64image | \
        alignleft aligncenter alignright alignjustify \
        bullist numlist outdent indent | removeformat | code'
        };
        tinymce.init(param);
    }

    initTags() {
        this.tags = [
            {
                id: `<span class="mceNonEditable"><tag data-tag-name="url" data-tag-type="button" data-tag-title="Accédez aux documents">${this.translate.instant('lang.accessLink')}</tag></span>`,
                label : 'lang.accessLink'
            },
            {
                id: `<span class="mceNonEditable"><tag data-tag-name="recipient.lastname" data-tag-type="string">${this.translate.instant('lang.recipientLastname')}</tag></span>`,
                label : 'lang.recipientLastname'
            },
            {
                id: `<span class="mceNonEditable"><tag data-tag-name="recipient.firstname" data-tag-type="string">${this.translate.instant('lang.recipientFirstname')}</tag></span>`,
                label : 'lang.recipientFirstname'
            },
            {
                id: `<span class="mceNonEditable"><tag data-tag-name="procedure.expiresAt" data-tag-type="date" data-tag-date-format="SHORT" data-tag-time-format="NONE" data-tag-locale="fr_FR">${this.translate.instant('lang.expiresAt')}</tag></span>`,
                label : 'lang.expiresAt'
            }
        ];
    }

    getConfig(id: any) {
        return new Promise((resolve) => {
            this.http.get('../rest/connectors/' + id)
                .pipe(
                    map((data: any) => data.otp),
                    tap((data: any) => {
                        this.connector = data;
                        this.connectorClone = JSON.parse(JSON.stringify(this.connector));
                        this.title = this.connector.label;
                        setTimeout(() => {
                            this.initMce();
                        }, 0);
                    }),
                    finalize(() => {
                        this.loading = false;
                    }),
                    catchError((err: any) => {
                        this.notificationService.handleErrors(err);
                        return of(false);
                    })
                )
                .subscribe();
        });
    }

    getConnectorTypes() {
        this.connectorTypes = this.otpService.getConnectorTypes().map((item: any) => ({
            id: item,
            logo: null
        }));

        this.connectorTypes.forEach(async element => {
            element.logo = await this.otpService.getUserOtpIcon(element.id);
        });
    }

    canValidate() {

        if (this.connector.label === this.connectorClone.label) {
            return false;
        } else {
            return true;
        }
    }

    onSubmit() {

        this.connector.message.otp_sms = this.connector.securityModes.indexOf('sms') === -1 ? '' : this.connector.message.otp_sms;

        this.connector.message.notification.body = tinymce.get('email_message').getContent();

        const regex = /data-tag-name=\"url\"/g;

        let error = false;

        if (tinymce.get('email_message').getContent().match(regex) === null) {
            this.notificationService.error(this.translate.instant('lang.emailLinkTagMandatory'));
            error = true;
        }

        const regex2 = /\{\{code\}\}/g;
        if (this.connector.securityModes.indexOf('sms') > -1 && this.connector.message.otp_sms.match(regex2) === null) {
            this.notificationService.error(this.translate.instant('lang.smsCodeTagMandatory'));
            error = true;
        }
        if (!error) {
            if (this.creationMode) {
                this.createconnector();
            } else {
                this.modifyconnector();
            }
        }
    }

    modifyconnector() {
        this.loading = true;
        this.http.put('../rest/connectors/' + this.connector.id, this.connector)
            .pipe(
                tap(() => {
                    this.router.navigate(['/administration/otps']);
                    this.notificationService.success('lang.connectorUpdated');
                }),
                catchError((err: any) => {
                    this.notificationService.handleErrors(err);
                    return of(false);
                })
            )
            .subscribe();
    }

    createconnector() {
        this.loading = true;
        this.http.post('../rest/connectors', this.connector)
            .pipe(
                tap((data: any) => {
                    this.router.navigate(['/administration/otps']);
                    this.notificationService.success('lang.connectorAdded');
                }),
                catchError((err: any) => {
                    this.notificationService.handleErrors(err);
                    return of(false);
                })
            )
            .subscribe();
    }

    async checkConnectorIsUsed(connectorId: any) {
        return new Promise(async (resolve) => {
            this.http.get('../rest/connectors/' + connectorId + '/currentVisa').pipe(
                tap((data: any) => {
                    this.nbCurrentWorkflow = data.nbCurrentWorkflow;
                    resolve(true);
                })
            ).subscribe();
        });
    }

    async deleteconnector() {
        await this.checkConnectorIsUsed(this.connector.id);
        const alert = await this.alertController.create({
            // cssClass: 'custom-alert-danger',
            header: this.translate.instant('lang.confirmMsg'),
            message: this.nbCurrentWorkflow === 0 ? this.translate.instant('lang.connectorNotUsed') : this.translate.instant('lang.connectorIsUsed', { number: this.nbCurrentWorkflow }),
            buttons: [
                {
                    text: this.translate.instant('lang.no'),
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => { }
                },
                {
                    text: this.translate.instant('lang.yes'),
                    handler: () => {
                        this.http.delete('../rest/connectors/' + this.connector.id)
                            .pipe(
                                tap(() => {
                                    this.router.navigate(['/administration/otps']);
                                    this.notificationService.success('lang.connectorDeleted');
                                }),
                                catchError((err: any) => {
                                    this.notificationService.handleErrors(err);
                                    return of(false);
                                })
                            )
                            .subscribe();
                    }
                }
            ]
        });

        await alert.present();
    }

    toggleSecurityMode(ev: any) {
        if (ev.checked) {
            this.connector.securityModes.push(ev.value);
        } else {
            const index = this.connector.securityModes.indexOf(ev.value);
            this.connector.securityModes.splice(index, 1);
        }
    }

    isCheckedSecurityMode(value: string) {
        return this.connector.securityModes.indexOf(value) > -1;
    }

    cancel() {
        this.router.navigate(['/administration/otps']);
    }

    addSmsVariable(code: string) {
        this.connector.message.otp_sms += ' {{' + code + '}}';
    }

    addEmailVariable(code: string) {
        tinymce.get('email_message').execCommand('mceInsertContent', false, ' ' + code + ' ');
    }
}
