import { Component, ViewChild } from '@angular/core';
import { SignaturesContentService } from '../../service/signatures.service';
import { NotificationService } from '../../service/notification.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { map, finalize, tap } from 'rxjs/operators';
import { LatinisePipe } from 'ngx-pipes';
import { AlertController } from '@ionic/angular';
import { OtpService } from '../../document/visa-workflow/otps/otp.service';


export interface OtpConnector {
    id: number;
    type: string;
    label: string;
    logo: string;
}

@Component({
    selector: 'app-administration-otp-list',
    templateUrl: 'otp-list.component.html',
    styleUrls: ['../administration.scss', 'otp-list.component.scss'],
})

export class OtpListComponent {

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    otpList: OtpConnector[] = [];
    sortedData: any[];
    dataSource: MatTableDataSource<OtpConnector>;
    displayedColumns: string[];
    loading: boolean = true;
    nbCurrentWorkflow: number;

    constructor(
        public http: HttpClient,
        private translate: TranslateService,
        private latinisePipe: LatinisePipe,
        public dialog: MatDialog,
        public signaturesService: SignaturesContentService,
        public notificationService: NotificationService,
        public alertController: AlertController,
        private otpService: OtpService,
    ) {
        this.displayedColumns = ['type', 'label', 'actions'];
    }

    applyFilter(filterValue: string) {
        filterValue = this.latinisePipe.transform(filterValue.toLowerCase());

        this.sortedData = this.otpList.filter(
            (option: any) => {
                let state = false;
                this.displayedColumns.forEach(element => {
                    if (option[element] && this.latinisePipe.transform(option[element].toLowerCase()).includes(filterValue)) {
                        state = true;
                    }
                });
                return state;
            }
        );
    }

    async ionViewWillEnter() {
        this.http.get('../rest/connectors')
            .pipe(
                map((data: any) => data.otp),
                finalize(() => this.loading = false)
            )
            .subscribe({
                next: async data => {
                    this.otpList = data;
                    for (let index = 0; index < this.otpList.length; index++) {
                        this.otpList[index].logo = await this.otpService.getUserOtpIcon(this.otpList[index].type);
                    }
                    this.sortedData = this.otpList.slice();
                },
            });
    }

    async checkConnectorIsUsed(connectorId: number) {
        return new Promise(async (resolve) => {
            this.http.get('../rest/connectors/' + connectorId + '/currentVisa').pipe(
                tap((data: any) => {
                    this.nbCurrentWorkflow = data.nbCurrentWorkflow;
                    resolve(true);
                })
            ).subscribe();
        });
    }

    async delete(connectorToDelete: OtpConnector) {
        await this.checkConnectorIsUsed(connectorToDelete.id);
        const alert = await this.alertController.create({
            // cssClass: 'custom-alert-danger',
            header: this.translate.instant('lang.confirmMsg'),
            message: this.nbCurrentWorkflow === 0 ? this.translate.instant('lang.connectorNotUsed') : this.translate.instant('lang.connectorIsUsed', { number: this.nbCurrentWorkflow }),
            buttons: [
                {
                    text: this.translate.instant('lang.no'),
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => { }
                },
                {
                    text: this.translate.instant('lang.yes'),
                    handler: () => {
                        this.http.delete('../rest/connectors/' + connectorToDelete.id)
                            .pipe(
                                finalize(() => this.loading = false)
                            )
                            .subscribe({
                                next: data => {
                                    const indexToDelete = this.otpList.findIndex(group => group.id === connectorToDelete.id);
                                    this.otpList.splice(indexToDelete, 1);
                                    this.sortedData = this.otpList.slice();
                                    this.notificationService.success('lang.connectorDeleted');

                                },
                            });
                    }
                }
            ]
        });

        await alert.present();
    }

    sortData(sort: Sort) {
        const data = this.otpList.slice();
        if (!sort.active || sort.direction === '') {
            this.sortedData = data;
            return;
        }

        this.sortedData = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            return compare(a[sort.active], b[sort.active], isAsc);
        });
    }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
