import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SignaturesContentService } from '../../service/signatures.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../service/auth.service';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificationService } from '../../service/notification.service';
import { AlertController, IonReorderGroup, ModalController, PopoverController } from '@ionic/angular';
import { ItemReorderEventDetail } from '@ionic/core';
import { TranslateService } from '@ngx-translate/core';
import { OtpCreateComponent } from './otps/otp-create.component';
import { OtpService } from './otps/otp.service';
import { VisaWorkflowModelsComponent } from './models/visa-workflow-models.component';
import { VisaWorkflowService } from '../../service/visa-workflow.service';

@Component({
    selector: 'app-visa-workflow',
    templateUrl: 'visa-workflow.component.html',
    styleUrls: ['visa-workflow.component.scss'],
})
export class VisaWorkflowComponent implements OnInit {

    @Input() editMode: boolean = false;
    @Input() visaWorkflow: any = [];
    @ViewChild(IonReorderGroup) reorderGroup: IonReorderGroup;

    loading: boolean = false;

    visaUsersSearchVal: string = '';
    visaUsersList: any = [];
    showVisaUsersList: boolean = false;
    customPopoverOptions = {
        header: 'Roles'
    };
    roles: any[] = [];
    hasConnector: boolean = false;

    constructor(
        public http: HttpClient,
        private translate: TranslateService,
        public alertController: AlertController,
        public signaturesService: SignaturesContentService,
        public authService: AuthService,
        public notificationService: NotificationService,
        public modalController: ModalController,
        private otpService: OtpService,
        public popoverController: PopoverController,
        public visaWorkflowService: VisaWorkflowService
    ) { }

    async ngOnInit(): Promise<void> {
        this.visaWorkflow.forEach((element: any, index: number) => {
            this.getAvatarUser(index);
        });
        if (this.editMode) {
            await this.getConnectors();
        }
    }

    doReorder(ev: CustomEvent<ItemReorderEventDetail>) {
        if (this.canMoveUser(ev)) {
            this.visaWorkflow = ev.detail.complete(this.visaWorkflow);
        } else {
            this.notificationService.error('lang.errorUserSignType');
            ev.detail.complete(false);
        }
    }

    canMoveUser(ev: CustomEvent<ItemReorderEventDetail>) {
        let newWorkflow = this.array_move(this.visaWorkflow.slice(), ev.detail.from, ev.detail.to);
        newWorkflow = newWorkflow.filter((item: any) => item !== undefined);
        const res = this.isValidWorkflow(newWorkflow);
        return res;
    }

    isValidWorkflow(workflow: any = this.visaWorkflow) {
        let res: boolean = true;
        workflow.forEach((item: any, indexUserRgs: number) => {
            if (['visa', 'stamp'].indexOf(item.role) === -1) {
                if (workflow.filter((itemUserStamp: any, indexUserStamp: number) => indexUserStamp > indexUserRgs && itemUserStamp.role === 'stamp').length > 0) {
                    res = false;
                }
            }
        });
        return res;
    }

    array_move(arr: any, old_index: number, new_index: number) {
        if (new_index >= arr.length) {
            let k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; // for testing
    }

    getVisaUsers(ev: any) {
        this.showVisaUsersList = true;
        if (ev.detail.value === '') {
            this.resetVisaUsersList();
        } else if (ev.detail.value.length >= 3) {
            this.http.get('../rest/autocomplete/users?search=' + ev.detail.value).pipe(
                tap((res: any) => {
                    this.visaUsersList = res;
                }),
                catchError(err => {
                    this.notificationService.handleErrors(err);
                    return of(false);
                })
            ).subscribe();
        }
    }

    addUser(user: any, searchInput: any) {
        this.resetVisaUsersList();

        user.signatureModes.unshift('visa');

        const userObj: any = {
            'userId': user.id,
            'userDisplay': `${user.firstname} ${user.lastname}`,
            'role': user.signatureModes[user.signatureModes.length - 1],
            'processDate': null,
            'current': false,
            'modes': user.signatureModes
        };
        this.visaWorkflow.push(userObj);
        if (!this.isValidWorkflow()) {
            this.visaWorkflow[this.visaWorkflow.length - 1].role = 'visa';
        }
        this.getAvatarUser(this.visaWorkflow.length - 1);
        this.visaUsersSearchVal = '';
        searchInput.setFocus();
    }

    removeUser(index: number) {
        this.visaWorkflow.splice(index, 1);
    }

    async getAvatarUser(index: number) {
        if (this.visaWorkflow[index].userId === null) {
            this.visaWorkflow[index].userPicture = await this.otpService.getUserOtpIcon('yousign');
        } else if (this.visaWorkflow[index].userPicture === undefined && this.visaWorkflow[index].userDisplay !== '') {
            this.http.get('../rest/users/' + this.visaWorkflow[index].userId + '/picture').pipe(
                tap((data: any) => {
                    this.visaWorkflow[index].userPicture = data.picture;
                }),
                catchError(err => {
                    this.notificationService.handleErrors(err);
                    return of(false);
                })
            ).subscribe();
        }
    }

    resetVisaUsersList() {
        this.visaUsersList = [];
    }

    async openOtpModal(item: any = null, position: number = null, userId: number = null) {
        if (this.editMode) {
            let objToSend: any;

            if (userId !== null) {
                objToSend = await this.getUserInfo(userId);
            } else if (item === null) {
                objToSend = null;
            } else {
                if (item.hasOwnProperty('status')) {
                    item = await this.formatData(item);
                }
                objToSend = {
                    firstname: item.otp.firstname,
                    lastname: item.otp.lastname,
                    email: item.otp.email,
                    phone: item.otp.phone,
                    security: item.otp.security,
                    sourceId: item.otp.sourceId,
                    type: item.otp.type,
                    role: item.role,
                    modes: item.otp.modes
                };
            }
            const modal = await this.modalController.create({
                cssClass: 'custom-modal',
                component: OtpCreateComponent,
                backdropDismiss: false,
                componentProps: {
                    data: objToSend
                }
            });
            await modal.present();

            modal.onDidDismiss()
                .then(async (result: any) => {
                    console.log(result);
                    if (result.data === 'cancel' && userId !== null) {
                        this.visaWorkflow[position].role = 'visa';
                    } else if (typeof result.data === 'object') {
                        const obj: any = {
                            'userId': null,
                            'userDisplay': `${result.data.firstname} ${result.data.lastname}`,
                            'userPicture': await this.otpService.getUserOtpIcon(result.data.type),
                            'role': result.data.role,
                            'processDate': null,
                            'current': false,
                            'modes': result.data.modes,
                            'otp': result.data
                        };
                        if (position !== null && item !== null) {
                            this.visaWorkflow[position] = obj;
                            this.notificationService.success(this.translate.instant('lang.modificationSaved'));
                        } else {
                            this.visaWorkflow.push(obj);
                        }
                    }
                });
        }
    }

    getUserInfo(userId: number) {
        return new Promise((resolve) => {
            this.http.get(`../rest/users/${userId}`).pipe(
                tap((data: any) => {
                    resolve({
                        firstname: data.user.firstname,
                        lastname: data.user.lastname,
                        email: data.user.email,
                        phone: data.user.phone,
                        modes: []
                    });
                }),
                catchError(err => {
                    this.notificationService.handleErrors(err);
                    resolve(false);
                    return of(false);
                })
            ).subscribe();
        });
    }

    getCurrentWorkflow() {
        return this.visaWorkflow;
    }

    getRole(id: string) {
        return this.authService.signatureRoles.filter((mode: any) => mode.id === id)[0];
    }

    loadWorkflow(workflow: any) {
        this.visaWorkflow = workflow;
        const length = this.visaWorkflow.length;
        for (let index = 0; index < length; index++) {
            this.getAvatarUser(index);
        }

    }

    isValidRole(indexWorkflow: any, role: string, currentRole: string) {
        if (this.visaWorkflow.filter((item: any, index: any) => index > indexWorkflow && ['stamp'].indexOf(item.role) > -1).length > 0 && ['visa', 'stamp'].indexOf(currentRole) > -1 && ['visa', 'stamp'].indexOf(role) === -1) {
            return false;
        } else if (this.visaWorkflow.filter((item: any, index: any) => index < indexWorkflow && ['visa', 'stamp'].indexOf(item.role) === -1).length > 0 && role === 'stamp') {
            return false;
        } else {
            return true;
        }
    }

    loadVisaWorkflow(model: any) {
        this.http.get(`../rest/workflowTemplates/${model.id}`).pipe(
            tap((data: any) => {
                const workflows: any[] = data.workflowTemplate.items.map((item: any) => {
                    item.mode = item.mode === 'stamp' && item.userId === null ? 'sign' : item.mode;
                    const obj: any = {
                        'userId': item.userId,
                        'userDisplay': item.userLabel,
                        'role': item.mode === 'visa' ? 'visa' : item.signatureMode,
                        'processDate': null,
                        'current': false,
                        'modes': ['visa'].concat(item.userSignatureModes)
                    };
                    return obj;
                });
                this.visaWorkflow = this.visaWorkflow.concat(workflows);
                this.visaWorkflow.forEach((element: any, index: number) => {
                    this.getAvatarUser(index);
                });
            }),
            catchError(err => {
                this.notificationService.handleErrors(err);
                return of(false);
            })
        ).subscribe();
    }

    getConnectors() {
        return new Promise((resolve) => {
            this.http.get('../rest/connectors').pipe(
                tap((data: any) => {
                    this.hasConnector = data.otp.length > 0 ? true : false;
                    const connectorIds: any[] = data.otp.map((connector: any) => connector.id);
                    resolve(connectorIds);
                }),
                catchError(err => {
                    this.notificationService.handleErrors(err);
                    return of(false);
                })
            ).subscribe();
        });
    }

    async openVisaWorkflowModels(ev: any) {
        const popover = await this.popoverController.create({
            component: VisaWorkflowModelsComponent,
            componentProps: { currentWorkflow: this.visaWorkflow },
            event: ev,
        });
        await popover.present();

        popover.onDidDismiss()
            .then((result: any) => {
                if (result.role !== 'backdrop') {
                    this.visaWorkflow = this.visaWorkflow.concat(result.data);
                    this.visaWorkflow.forEach((element: any, index: number) => {
                        this.getAvatarUser(index);
                    });
                }
            });
    }

    async formatData(item: any) {
        if (item.externalInformations) {
            item.role = item.role === 'stamp' ? 'sign' : item.role;
            return new Promise(async (resolve) => {
                let objToSend: any = {
                    otp: {
                        firstname: item.externalInformations.firstname,
                        lastname: item.externalInformations.lastname,
                        email: item.externalInformations.email,
                        phone: item.externalInformations.phone,
                        security: item.externalInformations.informations ? item.externalInformations.informations.security : item.externalInformations.security,
                        sourceId: item.externalInformations.external_signatory_book_id,
                        type: item.externalInformations.informations ? item.externalInformations.informations.type : item.externalInformations.type,
                        role: item.role !== undefined ? item.role : item.mode,
                        modes: ['visa', 'sign']
                    },
                    userId: null,
                    userDisplay: `${item.externalInformations.firstname} ${item.externalInformations.lastname}`,
                    userPicture: await this.otpService.getUserOtpIcon(item.externalInformations.informations ? item.externalInformations.informations.type : item.externalInformations.type),
                    role: item.role,
                    processDate: null,
                    current: false,
                    modes: ['visa', 'sign'],
                };
                const connectorIds: any = await this.getConnectors();
                if (connectorIds.indexOf(objToSend.otp.sourceId) === -1) {
                    objToSend.otp.sourceId = connectorIds[0];
                    objToSend = {
                        ...objToSend,
                        noConnector: true
                    };
                }
                this.visaWorkflow[this.visaWorkflow.indexOf(item)] = objToSend;
                resolve(objToSend);
            });
        }
    }

    checkRole(item: any, position: number, mode: string) {
        if (mode === 'external') {
            this.openOtpModal(item, position, item.userId);
        }
    }

    getProcessInfo(mode: string) {
        const isOtpSign = /otp_sign_/g;
        const isOtpvisa = /otp_visa_/g;
        if (mode.match(isOtpSign) !== null) {
            return 'sign';
        } else if (mode.match(isOtpvisa) !== null) {
            return 'visa';
        } else {
            return mode;
        }
    }
}
