import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController, PopoverController } from '@ionic/angular';
import { NotificationService } from '../../../service/notification.service';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthService } from '../../../service/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { VisaWorkflowService } from '../../../service/visa-workflow.service';

@Component({
    selector: 'app-visa-workflow-models',
    templateUrl: 'visa-workflow-models.component.html',
    styleUrls: ['visa-workflow-models.component.scss'],
})
export class VisaWorkflowModelsComponent implements OnInit {

    @Input() currentWorkflow: any[] = [];

    visaWorkflowModels: any[] = [];

    constructor(
        public http: HttpClient,
        private translate: TranslateService,
        public popoverController: PopoverController,
        public alertController: AlertController,
        public notificationService: NotificationService,
        public authService: AuthService,
        public visaWorkflowService: VisaWorkflowService
    ) { }

    ngOnInit(): void {
        this.visaWorkflowService.getVisaUserModels(this.currentWorkflow);
    }

    loadVisaWorkflow(model: any) {
        this.http.get(`../rest/workflowTemplates/${model.id}`).pipe(
            tap((data: any) => {
                const workflows: any[] = data.workflowTemplate.items.map((item: any) => {
                    const obj: any = {
                        'userId': item.userId,
                        'userDisplay': item.userLabel,
                        'role': item.mode === 'visa' ? 'visa' : item.signatureMode,
                        'processDate': null,
                        'current': false,
                        'modes': ['visa'].concat(item.userSignatureModes)
                    };
                    return obj;
                });
                this.popoverController.dismiss(workflows);
            }),
            catchError(err => {
                this.notificationService.handleErrors(err);
                return of(false);
            })
        ).subscribe();
    }
}
