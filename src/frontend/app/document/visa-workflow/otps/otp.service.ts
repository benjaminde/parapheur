import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import { NotificationService } from '../../../service/notification.service';

@Injectable({
    providedIn: 'root'
})
export class OtpService {

    otpConnectorTypes: string[] = [
        'yousign'
    ];

    private eventAction = new Subject<any>();

    constructor(
        public http: HttpClient,
        public notificationService: NotificationService,
    ) { }

    catchEvent(): Observable<any> {
        return this.eventAction.asObservable();
    }

    setEvent(content: any) {
        return this.eventAction.next(content);
    }

    getUserOtpIcon(id: string): Promise<string> {
        return new Promise((resolve) => {
            this.http.get(`assets/${id}.png`, { responseType: 'blob' }).pipe(
                tap((response: any) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(response);
                    reader.onloadend = () => {
                        resolve(reader.result as any);
                    };
                }),
                catchError(err => {
                    this.notificationService.handleErrors(err);
                    return of(false);
                })
            ).subscribe();
        });
    }

    getConnectorTypes() {
        return this.otpConnectorTypes;
    }
}
