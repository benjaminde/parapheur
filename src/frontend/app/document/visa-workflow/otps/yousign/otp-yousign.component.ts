import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../../service/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { NgForm } from '@angular/forms';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { OtpService } from '../otp.service';

@Component({
    selector: 'app-otp-yousign',
    templateUrl: 'otp-yousign.component.html',
    styleUrls: ['otp-yousign.component.scss'],
})
export class OtpYousignComponent implements OnInit {
    @ViewChild('otpForm', { static: false }) otpForm: NgForm;

    @Input() connectorId: string;
    @Input() otpYousign: any;

    securityModes: any[] = [];

    roles: any[] = ['otp_visa_yousign', 'otp_sign_yousign'];

    otp: any = {
        type: 'yousign',
        firstname: '',
        lastname: '',
        email: '',
        phone: '',
        security: 'sms',
        role: 'otp_sign_yousign',
        sourceId: '',
        modes: this.roles
    };

    constructor(
        public http: HttpClient,
        private translate: TranslateService,
        public notificationService: NotificationService,
        public otpService: OtpService
    ) {
        this.otpService.catchEvent().subscribe(async (res) => {
            if (res.id === 'connector') {
                this.connectorId = res.connectorId;
                await this.getConfig();
            }
        });
    }

    async ngOnInit(): Promise<void> {
        this.otp.sourceId = this.otpYousign ? this.otpYousign.sourceId : this.connectorId;
        await this.getConfig();
    }

    getConfig() {
        return new Promise((resolve) => {
            this.http.get('../rest/connectors/' + this.connectorId).pipe(
                tap((data: any) => {
                    this.securityModes = [... new Set(data.otp.securityModes)];
                    this.otp.security = this.securityModes[0];
                    if (this.otpYousign) {
                        this.otp = {...this.otp, ...this.otpYousign};
                        this.otp.modes = this.roles;
                        this.otp.sourceId = this.connectorId;
                    }
                    resolve(true);
                }),
                catchError(err => {
                    this.notificationService.handleErrors(err);
                    return of(false);
                })
            ).subscribe();
        });
    }

    getData() {
        return this.otp;
    }

    getSecurityMode() {
        return this.translate.instant('lang.' + this.otp.security);
    }

    isValid() {
        return this.otpForm.valid;
    }

    formatPhone() {
        if (this.otp.phone.length > 1 && this.otp.phone[0] === '0') {
            this.otp.phone = this.otp.phone.replace('0', '+33');
        }
    }

    getRegexPhone() {
        // map country calling code with national number length
        const phonesMap = {
            '32': [8, 10],      // Belgium
            '41': [4, 12],      // Swiss
            '44': [7, 10],      // United Kingdom
            '352': [4, 11],     // Luxembourg
            '351': [9, 11],     // Portugal
            '33': 9,            // France
            '1' : 10,           // USA
            '39': 11,           // Italy
            '34': 9             // Spain
        };
        const regex = Object.keys(phonesMap).reduce((phoneFormats: any [], countryCode: any) => {
            const numberLength = phonesMap[countryCode];
            if (Array.isArray(numberLength)) {
                phoneFormats.push('(\\+' + countryCode + `[0-9]\{${numberLength[0]},${numberLength[1]}\})`);
            } else {
                phoneFormats.push('(\\+' + countryCode + `[0-9]\{${numberLength}\})`);
            }
            return phoneFormats;
        }, []).join('|');
        return new RegExp(`^(${regex})$`);
    };
}
