import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../service/notification.service';
import { AuthService } from '../../../service/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { ModalController } from '@ionic/angular';
import { OtpYousignComponent } from './yousign/otp-yousign.component';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { OtpService } from './otp.service';

@Component({
    selector: 'app-otp-create',
    templateUrl: 'otp-create.component.html',
    styleUrls: ['otp-create.component.scss'],
})
export class OtpCreateComponent implements OnInit {

    @ViewChild('appOtpYousign', { static: false }) appOtpYousign: OtpYousignComponent;

    @Input() data: any;

    sources: any[] = [];

    currentSource: any = null;
    connectorId: number;

    loading: boolean = false;

    constructor(
        public http: HttpClient,
        private translate: TranslateService,
        public notificationService: NotificationService,
        public authService: AuthService,
        public modalController: ModalController,
        public otpService: OtpService
    ) { }

    async ngOnInit(): Promise<void> {
        await this.getConfig();
    }

    getConfig() {
        this.loading = true;
        return new Promise((resolve) => {
            this.http.get('../rest/connectors').pipe(
                tap((data: any) => {
                    this.sources = data.otp;
                    if (this.data?.type === undefined) {
                        this.currentSource = this.sources[0];
                        this.connectorId = this.currentSource.id;
                    } else {
                        this.currentSource = {
                            sourceId : this.data.sourceId,
                            type: this.data.type
                        };
                        this.connectorId = this.currentSource.sourceId;
                    }
                    this.loading = false;
                    resolve(true);
                }),
                catchError(err => {
                    this.notificationService.handleErrors(err);
                    return of(false);
                })
            ).subscribe();
        });
    }

    onSubmit() {
        this.modalController.dismiss(this.appOtpYousign.getData());
    }

    dismissModal() {
        this.modalController.dismiss('cancel');
    }

    isValid() {
        return this.appOtpYousign?.isValid();
    }

    setCurrerentSource(id: any) {
        this.otpService.setEvent({id: 'connector', connectorId: id});
    }
}
