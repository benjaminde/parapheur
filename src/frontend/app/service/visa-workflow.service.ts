import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, ModalController, PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { NotificationService } from './notification.service';

@Injectable()
export class VisaWorkflowService {

    visaWorkflowModels: any[] =  [];
    notExternalUsers: number = 0;

    constructor(
        public http: HttpClient,
        public authService: AuthService,
        public alertController: AlertController,
        private translate: TranslateService,
        public notificationService: NotificationService,
        public modalController: ModalController,
        public popoverController: PopoverController,

    ) { }

    getVisaUserModels(visaWorkflow: any[]) {
        this.http.get('../rest/workflowTemplates').pipe(
            tap((data: any) => {
                this.visaWorkflowModels = data.workflowTemplates;
                this.notExternalUsers = visaWorkflow.filter((user: any) => user.userId !== null).length;
            }),
            catchError(err => {
                this.notificationService.handleErrors(err);
                return of(false);
            })
        ).subscribe();
    }

    async createModel(currentWorkflow: any[]) {
        const checkExternalUsers: number = currentWorkflow.filter((user: any) => user.userId === null).length;
        const alert = await this.alertController.create({
            header: this.translate.instant('lang.newTemplate'),
            message: this.translate.instant(checkExternalUsers === 0 ? 'lang.newTemplateDesc' : 'lang.newTemplateDescWithOtp'),
            inputs: [
                {
                    name: 'title',
                    type: 'text',
                    placeholder: this.translate.instant('lang.label') + ' *',
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('lang.cancel'),
                    role: 'cancel',
                    handler: () => { }
                }, {
                    text: this.translate.instant('lang.validate'),
                    handler: (data: any) => {
                        if (data.title !== '') {
                            this.saveModel(data.title, currentWorkflow);
                            return true;
                        } else {
                            this.notificationService.error(this.translate.instant('lang.label') + ' ' + this.translate.instant('lang.mandatory'));
                            return false;
                        }
                    }
                }
            ]
        });

        await alert.present();
    }

    saveModel(title: string, currentWorkflow: any[]) {
        const objToSend: any = {
            title: title,
            items: currentWorkflow.filter((user: any) => user.userId !== null).map((item: any) => ({
                userId: item.userId,
                mode: this.authService.getWorkflowMode(item.role),
                signatureMode: this.authService.getSignatureMode(item.role)
            }))
        };
        this.http.post('../rest/workflowTemplates', objToSend).pipe(
            tap((res: any) => {
                this.notificationService.success('lang.modelCreated');
                this.visaWorkflowModels.push({ id: res.id, title: title });
            }),
            catchError(err => {
                this.notificationService.handleErrors(err);
                return of(false);
            })
        ).subscribe();
    }

    async removeModel(model: any) {
        const alert = await this.alertController.create({
            header: this.translate.instant('lang.delete'),
            message: this.translate.instant('lang.deleteTemplate'),
            buttons: [
                {
                    text: this.translate.instant('lang.no'),
                    role: 'cancel',
                    handler: () => { }
                }, {
                    text: this.translate.instant('lang.yes'),
                    handler: () => {
                        this.http.delete(`../rest/workflowTemplates/${model.id}`).pipe(
                            tap(() => {
                                this.visaWorkflowModels = this.visaWorkflowModels.filter((item: any) => item.id !== model.id);
                                this.notificationService.success(`Modèle ${model.title} supprimé`);
                            }),
                            catchError(err => {
                                this.notificationService.handleErrors(err);
                                return of(false);
                            })
                        ).subscribe();
                    }
                }
            ]
        });

        await alert.present();
    }

}
