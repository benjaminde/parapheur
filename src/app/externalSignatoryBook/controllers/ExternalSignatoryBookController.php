<?php

/**
* Copyright Maarch since 2008 under license.
* See LICENSE.txt file at the root folder for more details.
* This file is part of Maarch software.
*
*/

/**
* @brief ExternalSignatoryBook Controller
* @author dev@maarch.org
*/

namespace ExternalSignatoryBook\controllers;

use Document\models\DocumentModel;
use ExternalSignatoryBook\models\ExternalSignatoryBookModel;
use Group\controllers\PrivilegeController;
use History\controllers\HistoryController;
use Respect\Validation\Validator;
use Slim\Http\Request;
use Slim\Http\Response;
use Workflow\controllers\WorkflowController;
use Workflow\models\WorkflowModel;

class ExternalSignatoryBookController
{
    public function get(Request $request, Response $response)
    {
        $connectors = ExternalSignatoryBookModel::get(['select' => ['id', 'label', 'type', 'otp_code'], 'orderBy' => ['label']]);
        foreach ($connectors as $key => $data) {
            $connectors[$key]['securityModes'] = json_decode($data['otp_code'], true);
            unset($connectors[$key]['otp_code']);
        }

        return $response->withJson(['otp' => $connectors]);
    }

    public function getById(Request $request, Response $response, array $args)
    {
        if (!Validator::intVal()->notEmpty()->validate($args['id'])) {
            return $response->withStatus(400)->withJson(['errors' => 'Route id must be an integer']);
        }

        $select = [];
        if (!PrivilegeController::hasPrivilege(['userId' => $GLOBALS['id'], 'privilege' => 'manage_otp_connectors'])) {
            $select = ['otp_code'];
        }

        $connector = ExternalSignatoryBookModel::getById(['select' => $select, 'id' => $args['id']]);
        if (empty($connector)) {
            return $response->withStatus(400)->withJson(['errors' => 'Connector not found']);
        }

        $connector['securityModes'] = json_decode($connector['otp_code'], true);
        unset($connector['otp_code']);

        if (PrivilegeController::hasPrivilege(['userId' => $GLOBALS['id'], 'privilege' => 'manage_otp_connectors'])) {
            $connector['message'] = json_decode($connector['message_content'], true);
            unset($connector['message_content']);
    
            $connectionData = json_decode($connector['connection_data'], true);
            $connector['apiUri'] = $connectionData['apiUri'];
            $connector['apiKey'] = $connectionData['apiKey'];
            unset($connector['connection_data']);
        }

        return $response->withJson(['otp' => $connector]);
    }

    public function create(Request $request, Response $response)
    {
        if (!PrivilegeController::hasPrivilege(['userId' => $GLOBALS['id'], 'privilege' => 'manage_otp_connectors'])) {
            return $response->withStatus(403)->withJson(['errors' => 'Privilege forbidden']);
        }

        $body = $request->getParsedBody();

        $control = ExternalSignatoryBookController::control(['body' => $body]);
        if (!empty($control['errors'])) {
            return $response->withStatus(400)->withJson(['errors' => $control['errors']]);
        }

        $connectionData = ['apiUri' => $body['apiUri'], 'apiKey' => $body['apiKey']];

        $id = ExternalSignatoryBookModel::create([
            'label'           => $body['label'],
            'type'            => $body['type'],
            'connection_data' => json_encode($connectionData),
            'otp_code'        => json_encode($body['securityModes']),
            'message_content' => json_encode($body['message']),
        ]);

        HistoryController::add([
            'code'          => 'OK',
            'objectType'    => 'connectors',
            'objectId'      => $id,
            'type'          => 'CREATION',
            'message'       => "{connectorAdded} : {$body['label']}"
        ]);

        return $response->withJson(['id' => $id]);
    }

    public function update(Request $request, Response $response, array $aArgs)
    {
        if (!PrivilegeController::hasPrivilege(['userId' => $GLOBALS['id'], 'privilege' => 'manage_otp_connectors'])) {
            return $response->withStatus(403)->withJson(['errors' => 'Privilege forbidden']);
        }

        $body = $request->getParsedBody();

        if (!Validator::intVal()->notEmpty()->validate($aArgs['id'])) {
            return $response->withStatus(400)->withJson(['errors' => 'Id must be an integer']);
        }

        $control = ExternalSignatoryBookController::control(['body' => $body]);
        if (!empty($control['errors'])) {
            return $response->withStatus(400)->withJson(['errors' => $control['errors']]);
        }

        $connectionData = ['apiUri' => $body['apiUri'], 'apiKey' => $body['apiKey']];

        $connector = ExternalSignatoryBookModel::getById(['id' => $aArgs['id']]);
        if (empty($connector)) {
            return $response->withStatus(400)->withJson(['errors' => 'Connector not found']);
        }

        ExternalSignatoryBookModel::update([
            'set' => [
                'label'           => $body['label'],
                'type'            => $body['type'],
                'connection_data' => json_encode($connectionData),
                'otp_code'        => json_encode($body['securityModes']),
                'message_content' => json_encode($body['message']),
            ],
            'where' => ['id = ?'],
            'data'  => [$aArgs['id']]
        ]);

        HistoryController::add([
            'code'       => 'OK',
            'objectType' => 'connectors',
            'objectId'   => $aArgs['id'],
            'type'       => 'MODIFICATION',
            'message'    => "{connectorUpdated} : {$body['label']}"
        ]);

        return $response->withStatus(204);
    }

    public static function control(array $args)
    {
        if (empty($args['body'])) {
            return ['errors' => 'Body is not set or empty'];
        } elseif (!Validator::stringType()->notEmpty()->length(1, 255)->validate($args['body']['label'])) {
            return ['errors' => 'Body label is empty or not a string or longer than 255 caracteres'];
        } elseif (!Validator::stringType()->notEmpty()->length(1, 128)->validate($args['body']['type'])) {
            return ['errors' => 'Body type is empty or not a string or longer than 128 caracteres'];
        } elseif (!Validator::arrayType()->notEmpty()->validate($args['body']['securityModes'])) {
            return ['errors' => 'Body securityModes is empty or not an array'];
        }

        if ($args['body']['type'] != 'yousign') {
            return ['errors' => 'Only type yousign is allowed'];
        }

        $invalidOtpCode = array_diff($args['body']['securityModes'], ['sms', 'email']);
        if (!empty($invalidOtpCode)) {
            return ['errors' => 'Only sms and/or email are allowed for securityModes'];
        }

        if ($args['body']['type'] == 'yousign') {
            if (empty($args['body']['apiUri']) || empty($args['body']['apiKey'])) {
                return ['errors' => 'Body apiUri or apiKey is empty'];
            }
            if (empty($args['body']['message']['notification']['subject']) || empty($args['body']['message']['notification']['body'])) {
                return ['errors' => 'Body message notification must have subject and body attributes'];
            }
            if (!empty($args['body']['message']['otp_sms']) && !Validator::stringType()->notEmpty()->length(1, 150)->validate($args['body']['message']['otp_sms'])) {
                return ['errors' => 'Body message otp_sms length must be less than 150 caracteres'];
            }
            if (!empty($args['body']['message']['otp_sms']) && strpos($args['body']['message']['otp_sms'], '{{code}}') === false) {
                return ['errors' => 'Body message otp_sms must contains {{code}}'];
            }
            if (in_array('sms', $args['body']['securityModes']) && empty($args['body']['message']['otp_sms'])) {
                return ['errors' => 'Body message otp_sms must be set'];
            }
        }

        return [];
    }

    public function delete(Request $request, Response $response, array $aArgs)
    {
        if (!PrivilegeController::hasPrivilege(['userId' => $GLOBALS['id'], 'privilege' => 'manage_otp_connectors'])) {
            return $response->withStatus(403)->withJson(['errors' => 'Privilege forbidden']);
        }

        if (!Validator::intVal()->notEmpty()->validate($aArgs['id'])) {
            return $response->withStatus(400)->withJson(['errors' => 'Id must be an integer']);
        }

        $connector = ExternalSignatoryBookModel::getById(['id' => $aArgs['id']]);
        if (empty($connector)) {
            return $response->withStatus(400)->withJson(['errors' => 'Connector not found']);
        }

        $workflows = WorkflowModel::getWorkflowWithExternalInfo([
            'select' => ['DISTINCT(w.main_document_id)'],
            'where'  => ['w.status is null', 'wei.external_signatory_book_id = ?'],
            'data'   => [$aArgs['id']]
        ]);
        $currentDocumentsId = array_column($workflows, 'main_document_id');

        if (!empty($currentDocumentsId)) {
            $documents = DocumentModel::get(['select' => ['typist', 'title', 'id'], 'where' => ['id in (?)'], 'data' => [$currentDocumentsId]]);
            foreach ($documents as $document) {
                $process = WorkflowController::interruptProcess(['id' => $document['id'], 'documentTypist' => $document['typist'], 'documentTitle' => $document['title']]);
                if (!empty($process['errors'])) {
                    return $response->withStatus(400)->withJson(['errors' => $process['errors']]);
                }
            }
        }

        ExternalSignatoryBookModel::delete(['where' => ['id = ?'], 'data' => [$aArgs['id']]]);

        HistoryController::add([
            'code'       => 'OK',
            'objectType' => 'connectors',
            'objectId'   => $aArgs['id'],
            'type'       => 'SUPPRESSION',
            'message'    => "{connectordeleted} : {$connector['label']}"
        ]);

        return $response->withStatus(204);
    }

    public function getCurrentVisa(Request $request, Response $response, array $aArgs)
    {
        if (!PrivilegeController::hasPrivilege(['userId' => $GLOBALS['id'], 'privilege' => 'manage_otp_connectors'])) {
            return $response->withStatus(403)->withJson(['errors' => 'Privilege forbidden']);
        }

        if (!Validator::intVal()->notEmpty()->validate($aArgs['id'])) {
            return $response->withStatus(400)->withJson(['errors' => 'Id must be an integer']);
        }

        $workflows = WorkflowModel::getWorkflowWithExternalInfo([
            'select' => ['DISTINCT(w.main_document_id)'],
            'where'  => ['w.status is null', 'wei.external_signatory_book_id = ?'],
            'data'   => [$aArgs['id']]
        ]);

        return $response->withJson(['nbCurrentWorkflow' => count($workflows)]);
    }
}
