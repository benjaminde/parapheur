<?php

/**
* Copyright Maarch since 2008 under license.
* See LICENSE.txt file at the root folder for more details.
* This file is part of Maarch software.
*
*/

/**
* @brief ExternalSignatoryBook Model
* @author dev@maarch.org
*/

namespace ExternalSignatoryBook\models;

use SrcCore\models\DatabaseModel;
use SrcCore\models\ValidatorModel;

class ExternalSignatoryBookModel
{
    public static function get(array $aArgs = [])
    {
        ValidatorModel::arrayType($aArgs, ['select', 'where', 'data', 'orderBy']);
        ValidatorModel::intType($aArgs, ['limit']);

        $externalData = DatabaseModel::select([
            'select'    => empty($aArgs['select']) ? ['*'] : $aArgs['select'],
            'table'     => ['external_signatory_book'],
            'where'     => empty($aArgs['where']) ? [] : $aArgs['where'],
            'data'      => empty($aArgs['data']) ? [] : $aArgs['data'],
            'orderBy'   => empty($aArgs['orderBy']) ? [] : $aArgs['orderBy'],
            'limit'     => empty($aArgs['limit']) ? 0 : $aArgs['limit']
        ]);

        return $externalData;
    }

    public static function getById(array $aArgs)
    {
        ValidatorModel::notEmpty($aArgs, ['id']);
        ValidatorModel::intVal($aArgs, ['id']);
        ValidatorModel::arrayType($aArgs, ['select']);

        $externalData = ExternalSignatoryBookModel::get([
            'select'    => empty($aArgs['select']) ? ['*'] : $aArgs['select'],
            'where'     => ['id = ?'],
            'data'      => [$aArgs['id']]
        ]);

        if (!empty($externalData)) {
            return $externalData[0];
        }

        return [];
    }

    public static function create(array $aArgs)
    {
        ValidatorModel::notEmpty($aArgs, ['label', 'type', 'connection_data', 'otp_code']);
        ValidatorModel::stringType($aArgs, ['label', 'type']);

        $nextSequenceId = DatabaseModel::getNextSequenceValue(['sequenceId' => 'external_signatory_book_id_seq']);
        DatabaseModel::insert([
            'table'         => 'external_signatory_book',
            'columnsValues' => [
                'id'              => $nextSequenceId,
                'label'           => $aArgs['label'],
                'type'            => $aArgs['type'],
                'connection_data' => $aArgs['connection_data'],
                'otp_code'        => $aArgs['otp_code'],
                'message_content' => $aArgs['message_content'],
            ]
        ]);

        return $nextSequenceId;
    }

    public static function update(array $args)
    {
        ValidatorModel::notEmpty($args, ['set', 'where', 'data']);
        ValidatorModel::arrayType($args, ['set', 'where', 'data']);

        DatabaseModel::update([
            'table' => 'external_signatory_book',
            'set'   => $args['set'],
            'where' => $args['where'],
            'data'  => $args['data']
        ]);

        return true;
    }

    public static function delete(array $args)
    {
        ValidatorModel::notEmpty($args, ['where', 'data']);
        ValidatorModel::arrayType($args, ['where', 'data']);

        DatabaseModel::delete([
            'table' => 'external_signatory_book',
            'where' => $args['where'],
            'data'  => $args['data']
        ]);

        return true;
    }
}
