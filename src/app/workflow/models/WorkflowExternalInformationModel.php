<?php

/**
* Copyright Maarch since 2008 under license.
* See LICENSE.txt file at the root folder for more details.
* This file is part of Maarch software.
*
*/

/**
* @brief WorkflowExternalInformationModel Model
* @author dev@maarch.org
*/

namespace Workflow\models;

use SrcCore\models\ValidatorModel;
use SrcCore\models\DatabaseModel;

class WorkflowExternalInformationModel
{
    public static function get(array $args)
    {
        ValidatorModel::notEmpty($args, ['select']);
        ValidatorModel::arrayType($args, ['select', 'where', 'data', 'orderBy', 'groupBy']);
        ValidatorModel::intType($args, ['limit', 'offset']);

        $workflows = DatabaseModel::select([
            'select'    => $args['select'],
            'table'     => ['workflows_external_informations'],
            'where'     => empty($args['where']) ? [] : $args['where'],
            'data'      => empty($args['data']) ? [] : $args['data'],
            'orderBy'   => empty($args['orderBy']) ? [] : $args['orderBy'],
            'groupBy'   => empty($args['groupBy']) ? [] : $args['groupBy'],
            'offset'    => empty($args['offset']) ? 0 : $args['offset'],
            'limit'     => empty($args['limit']) ? 0 : $args['limit'],
        ]);

        return $workflows;
    }

    public static function getByWorkflowId(array $args)
    {
        ValidatorModel::notEmpty($args, ['select', 'workflowId']);
        ValidatorModel::intVal($args, ['workflowId']);
        ValidatorModel::arrayType($args, ['select']);

        $workflow = DatabaseModel::select([
            'select'    => $args['select'],
            'table'     => ['workflows_external_informations'],
            'where'     => ['workflow_id = ?'],
            'data'      => [$args['workflowId']]
        ]);

        if (empty($workflow[0])) {
            return [];
        }

        return $workflow[0];
    }

    public static function create(array $args)
    {
        ValidatorModel::notEmpty($args, ['workflow_id', 'firstname', 'lastname']);
        ValidatorModel::intVal($args, ['workflow_id']);
        ValidatorModel::stringType($args, ['firstname', 'lastname', 'email', 'phone', 'informations']);

        DatabaseModel::insert([
            'table'         => 'workflows_external_informations',
            'columnsValues' => [
                'workflow_id'                   => $args['workflow_id'],
                'external_signatory_book_id'    => $args['external_signatory_book_id'],
                'firstname'                     => $args['firstname'],
                'lastname'                      => $args['lastname'],
                'email'                         => $args['email'],
                'phone'                         => $args['phone'],
                'informations'                  => $args['informations']
            ]
        ]);

        return true;
    }

    public static function update(array $args)
    {
        ValidatorModel::notEmpty($args, ['set', 'where', 'data']);
        ValidatorModel::arrayType($args, ['set', 'where', 'data']);

        DatabaseModel::update([
            'table' => 'workflows_external_informations',
            'set'   => $args['set'],
            'where' => $args['where'],
            'data'  => $args['data']
        ]);

        return true;
    }

    public static function delete(array $args)
    {
        ValidatorModel::notEmpty($args, ['where', 'data']);
        ValidatorModel::arrayType($args, ['where', 'data']);

        DatabaseModel::delete([
            'table' => 'workflows_external_informations',
            'where' => $args['where'],
            'data'  => $args['data']
        ]);

        return true;
    }
}
